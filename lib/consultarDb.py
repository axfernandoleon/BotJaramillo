from SPARQLWrapper import SPARQLWrapper2, JSON


class consultar:
    def __init__(self, dbC):
        # Variables de inicio
        self.sparql = SPARQLWrapper2(dbC)

    # Metodos
    def listaCantantes(self):
        self.sparql.setQuery("""
            select distinct ?uri ?nombre where {
            ?uri  rdf:type yago:WikicatEcuadorianSingers .
            ?uri foaf:name ?nombre .
            }
        """)
        dic = []
        for result in self.sparql.query().bindings:
            dic.append((result["nombre"].value, result["uri"].value))
        if len(dic) != 0:
            return dic
        else:
            # print("No encuentro el Pais")
            return 0

    def infoCantante(self,nombre):
        self.sparql.setQuery("""
            select distinct ?uri  ?nombre ?abs where {
            ?uri  rdf:type yago:WikicatEcuadorianSingers .
            ?uri foaf:name ?nombre .
            optional{ ?uri dbo:abstract ?abs .}
            FILTER(lang(?abs) = "es")
            FILTER(?nombre=\""""+nombre+"""\"@en)
            }
        """)
        dic = []
        for result in self.sparql.query().bindings:
            dic.append((result["uri"].value, result["abs"].value))
        if len(dic) != 0:
            return dic
        else:
            # print("No encuentro el Pais")
            return 0

    def consultaPais(self):
        self.sparql.setQuery("""
                select distinct ?nomPais ?nomCapital ?nomMoneda ?uriDemon
                where {
                ?pais rdf:type dbo:Country .
                ?pais rdfs:label ?nomPais .
                ?pais dbo:capital ?capital .
                ?capital rdfs:label ?nomCapital .
                ?pais dbo:currency ?moneda .
                ?moneda rdfs:label ?nomMoneda .
                ?pais dbo:demonym ?uriDemon .
                FILTER(lang(?nomMoneda)="es")
                FILTER(?nomPais="Ecuador"@es)
                FILTER(?nomCapital="Quito"@es)
                }
        """)
        dic = []
        for result in self.sparql.query().bindings:
            dic.append((result["nomPais"].value, result["nomCapital"].value, result["nomMoneda"].value, result["uriDemon"].value))
        if len(dic) != 0:
            return dic
        else:
            # print("No encuentro el Pais")
            return 0
    def consultaEtnia(self):
        self.sparql.setQuery("""
            select distinct ?nomPais ?nometnia ?infoetnia where {
            ?pais rdf:type dbo:Country .
            ?pais rdfs:label ?nomPais .
            ?pais dbo:ethnicGroup ?etnia .
            ?etnia rdfs:label ?nometnia .
            ?etnia rdfs:comment ?infoetnia .
            FILTER(lang(?nometnia)="es")
            FILTER(lang(?infoetnia)="es")
            FILTER(?nomPais="Ecuador"@es)
            }
        """)
        dic = []
        for result in self.sparql.query().bindings:
            dic.append((result["nomPais"].value, result["nometnia"].value))
        if len(dic) != 0:
            return dic
        else:
            # print("No encuentro el Pais")
            return 0

    def consultaInfoEtnia(self,nombre):
        self.sparql.setQuery("""
            select distinct ?nometnia ?infoetnia where {
            ?pais rdf:type dbo:Country .
            ?pais rdfs:label ?nomPais .
            ?pais dbo:ethnicGroup ?etnia .
            ?etnia rdfs:label ?nometnia .
            ?etnia rdfs:comment ?infoetnia .
            FILTER(?nometnia=\""""+nombre+"""\"@es)
            FILTER(lang(?infoetnia)="es")
            FILTER(?nomPais="Ecuador"@es)
            }
        """)
        dic = []
        for result in self.sparql.query().bindings:
            dic.append((result["nometnia"].value, result["infoetnia"].value))
        if len(dic) != 0:
            return dic
        else:
            # print("No encuentro el Pais")
            return 0
    