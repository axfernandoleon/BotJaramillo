import json
import lib.consultarDb as cdb
dbpedia=cdb.consultar("http://dbpedia.org/sparql")

def respondera(data):
    print(json.dumps(data, indent=2))
    if data["intent"] is not None:
        intent=data["intent"]["intentName"]
        if intent=="Saludo":
            return "Hola, te puedo ayudar?\nYo conozco de Ecuador y sus cantantes quieres conocerlos?"

        if intent=="ListaArtistas":
            slots= data["slots"]
            if len(slots)==2:

                respuesta="Los cantantes Ecuatorianos que conozco son:\n"
                return respuesta+listarArtistas()
            else: 
                #print(slots)
                return "Aun no tengo respuesta para esa pregunta\nTe sugiero preguntarme de los artistas de Ecuador eso si conozco "
        
        elif intent=="InformacionArtista":
            slots= data["slots"]
            #print(json.dumps(slots, indent=2))
            if len(slots)==2:

                nombre=slots[1]["value"]["value"]
                return infoArtista(nombre)
            else: 
                #print(slots)
                return "NO entendi :("
        
        elif intent=="InformacionPais":
            slots= data["slots"]
            if len(slots)==2:
                listC=dbpedia.consultaPais()
                #print(listC)
                slots=slots[0]["entity"]
                return infoPais(slots,listC)
            else: 
                #print(slots)
                return "Aun no tengo respuesta para esa pregunta\nPuedes consultar\nLa capital del Ecuador\nLa moneda del Ecuador\nLa denominacion de las personas de Ecuador"
        elif intent=="ListaEtnia":
            slots= data["slots"]
            if len(slots)==2:
                listC=dbpedia.consultaEtnia()
                #print(listC)
                slots=slots[0]["entity"]
                return listaEtnia(slots,listC)
            else:
                return "Aun no tengo respuesta para esa pregunta\nPuedes consultar\nLas etnias del Ecuador\ny la descripción de cada una de ellas"
        
            return infoPais(slots,listC)
        elif intent=="InfomacionEtnias":
            slots= data["slots"]
            if len(slots)==2:
                nombre=slots[1]["value"]["value"]
                listC=dbpedia.consultaInfoEtnia(nombre)
                
                if listC !=0:
                    respuesta=":D Te comento:\n"
                    for item , item2 in listC:
                        respuesta+=""+item2+"\n"
                    return respuesta
            
            else:
                return "Pregunta una de las etnias en la lista\n"
        else:
            return "Es un nuevo intent no registrado :D"
    else:
        return "Disculpa, aun estoy aprendiendo, me decias?"

def listarArtistas():

    listC=dbpedia.listaCantantes()
    listC.pop(3) 
    respuesta=""
    for item , item2 in listC:
        respuesta+="- "+item+"\n"
    return respuesta

def infoArtista(nombre):
    listC=dbpedia.infoCantante(nombre)
    if listC !=0:

        respuesta=":D Te comento:\n"
        for item , item2 in listC:
            respuesta+=""+item2+"\n"
        return respuesta

    else:
        return "Disculpa ese artista no lo conozco, pregunta uno de la lista\n"+listarArtistas()

def infoPais(slots,listC):
    respuesta=""
    for item , item2,item3,item4 in listC:
        if slots=="ecapital":
            respuesta+="La Capital es "+item2+"\n"
            
        elif slots=="emoneda":
            respuesta+="La moneda es el "+item3+"\n"
            
        elif slots=="ellaman":
            respuesta+="A los ciudadanos del Ecuador se les denomina "+item4+" claro en ingles es como lo tengo en mi base de conocimiento\n"
            
    return respuesta

def listaEtnia(slots,listC):
    respuesta=""
    for item , item2 in listC:
        if slots=="culturaE":
            respuesta+="- "+item2+"\n"
                
    return respuesta



